using System;
using AutoFixture;

namespace PetStore.Entities.Builders
{
    public class PetBuilder
    {
        private static readonly Fixture _fixture = new();
        private static Random random = new Random();
        public static Pet Get()
        {
            var pet = _fixture.Create<Pet>();
            pet.Id = random.Next(555, int.MaxValue);
            return pet;
        }
    }
}
