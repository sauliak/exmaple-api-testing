using NUnit.Framework;
using PetStore.Client;

namespace PetStore.Tests.Negative
{
    public class NegativeTests : BaseTest
    {
        [TestCase(-1)]
        [TestCase(23898753)]
        public void TryGetPetByWrongId(int id)
        {
            PetStoreService.GetPetById(id);
            Assert.IsFalse(PetStoreService.Response.IsSuccessful, $"Failed: {(int)PetStoreService.Response.StatusCode} : {PetStoreService.Response.StatusDescription}");
        }

        [Test]
        public void TryUpdatePetWithEmptyBody()
        {
            PetStoreService.UpdatePet();
            Assert.IsFalse(PetStoreService.Response.IsSuccessful, $"Failed: {(int)PetStoreService.Response.StatusCode} : {PetStoreService.Response.StatusDescription}");
        }

        [Test]
        public void TryAddNewPetWithEmptyBody()
        {
            PetStoreService.AddPet();
            Assert.IsFalse(PetStoreService.Response.IsSuccessful, $"Failed: {(int)PetStoreService.Response.StatusCode} : {PetStoreService.Response.StatusDescription}");
        }
    }
}
