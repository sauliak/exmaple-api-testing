using System;
using System.IO;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using PetStore.Client;
using Serilog;

namespace PetStore
{
    public class BaseTest
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            Log.Logger = new LoggerConfiguration().CreateLogger();
            Log.Logger = new LoggerConfiguration()
                .WriteTo.Console()
                .WriteTo.File($@"{Directory.GetCurrentDirectory()}..\..\..\..\Logs\PetStoreLog-.txt", rollingInterval: RollingInterval.Day, outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} [{Level:u3}] {Message:lj}{NewLine}{Exception}").CreateLogger();

            PetStoreService.Client = new (new Uri(Configuration.BaseURL));
        }

        [SetUp]
        public void SetUp()
        {
            Log.Information($"Starting new test: {TestContext.CurrentContext.Test.Name}");
        }

        [TearDown]
        public void TearDown()
        {
            if (TestContext.CurrentContext.Result.Outcome == ResultState.Success)
            {
                Log.Information($"Test {TestContext.CurrentContext.Test.Name} successfully passed!");
            }
            else
            {
                Log.Error($"Test {TestContext.CurrentContext.Test.Name} failed!");
            }
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            Log.CloseAndFlush();
            PetStoreService.Client.Dispose();
        }
    }
}
