using ApiTests;
using Httpbin.Client;
using NUnit.Framework;

namespace Httpbin.Tests.Negative
{
    public class NegativeTests : BaseTest
    {
        [TestCase(403)]
        [TestCase(400)]
        [TestCase(501)]
        public void TryGetFailedStatusCode(int code)
        {
            HttpbinService.GetStatusCode(code);
            Assert.IsFalse(HttpbinService.Response.IsSuccessful, $"Failed: {(int)HttpbinService.Response.StatusCode} : {HttpbinService.Response.StatusDescription}");
        }

        [TestCase("wronguser", "pass")]
        [TestCase("user", "wrongpass")]
        [TestCase("wronguser", "wrongpass")]
        public void TryAuthorizeWithWrongCredentials(string user, string pass)
        {
            HttpbinService.SetAuthorizationRequest("user", "pass");
            HttpbinService.SetAuthenticator(user, pass);
            HttpbinService.Authenticate();
            Assert.IsFalse(HttpbinService.Response.IsSuccessful, $"Failed: {(int)HttpbinService.Response.StatusCode} : {HttpbinService.Response.StatusDescription}");
        }
    }
}
