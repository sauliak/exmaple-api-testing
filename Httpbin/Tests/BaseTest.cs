using System;
using System.IO;
using Httpbin.Client;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using Serilog;

namespace ApiTests
{
    public class BaseTest
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            Log.Logger = new LoggerConfiguration().CreateLogger();
            Log.Logger = new LoggerConfiguration()
                .WriteTo.Console()
                .WriteTo.File(@$"{Directory.GetCurrentDirectory()}..\..\..\..\Logs\HttpBinLog-.txt", rollingInterval: RollingInterval.Day, outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} [{Level:u3}] {Message:lj}{NewLine}{Exception}").CreateLogger();

            HttpbinService.Client = new (new Uri(Configuration.BaseURL));
        }

        [SetUp]
        public void SetUp()
        {
            Log.Information($"Starting new test: {TestContext.CurrentContext.Test.Name}");
        }

        [TearDown]
        public void TearDown()
        {
            if (HttpbinService.Response.IsSuccessful)
            {
                HttpbinService.GetResponse();
            }

            if (TestContext.CurrentContext.Result.Outcome == ResultState.Success)
            {
                Log.Information($"Test {TestContext.CurrentContext.Test.Name} successfully passed!");
            }
            else
            {
                Log.Error($"Test {TestContext.CurrentContext.Test.Name} failed!");
            }
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            Log.CloseAndFlush();
            HttpbinService.Client.Dispose();
        }
    }
}
