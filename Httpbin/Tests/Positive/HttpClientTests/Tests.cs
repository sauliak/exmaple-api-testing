using System;
using System.Net.Http;
using ApiTests;
using NUnit.Framework;

namespace Httpbin.Tests.Positive.HttpClientTests
{
    [TestFixture]
    public class Tests
    {
        private HttpClient _httpClient;

        [SetUp]
        public void SetUp()
        {
            _httpClient = new ();
        }

        [Test]
        public void SimpleHttpClientTest()
        {
            var response = _httpClient.GetAsync($"{Configuration.BaseURL}/ip").Result;
            Console.WriteLine(response);
            Assert.That(response.IsSuccessStatusCode, Is.True, $"Failed: {(int)response.StatusCode} : {response.ReasonPhrase}");
        }

        [TearDown]
        public void TearDown()
        {
            _httpClient.Dispose();
        }
    }
}
