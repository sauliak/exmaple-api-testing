using ApiTests;
using Httpbin.Client;
using NUnit.Framework;
using RA;

namespace Httpbin.Tests.Positive
{
    public class PositiveTests : BaseTest
    {
        [Test]
        public void GetPositiveStatusCodeUsingPatch()
        {
            HttpbinService.GetStatusCodeUsingPatch(200);
            Assert.Multiple(() =>
            {
                Assert.That(HttpbinService.Response.IsSuccessful, Is.True, $"Failed: {(int)HttpbinService.Response.StatusCode} : {HttpbinService.Response.StatusDescription}");
                Assert.That((int)HttpbinService.Response.StatusCode, Is.EqualTo(200));
            });
        }

        [Test]
        public void GetIp()
        {
            HttpbinService.GetIp();
            Assert.Multiple(() =>
            {
                Assert.That(HttpbinService.Response.IsSuccessful, Is.True, $"Failed: {(int)HttpbinService.Response.StatusCode} : {HttpbinService.Response.StatusDescription}");
                Assert.That((int)HttpbinService.Response.StatusCode, Is.EqualTo(200));
            });
        }

        [Test]
        public void GetHeaders()
        {
            HttpbinService.GetHeaders();
            Assert.Multiple(() =>
            {
                Assert.That(HttpbinService.Response.IsSuccessful, Is.True, $"Failed: {(int)HttpbinService.Response.StatusCode} : {HttpbinService.Response.StatusDescription}");
                Assert.That((int)HttpbinService.Response.StatusCode, Is.EqualTo(200));
            });
        }

        [Test]
        public void GetUserAgent()
        {
            HttpbinService.GetUserAgent();
            Assert.Multiple(() =>
            {
                Assert.That(HttpbinService.Response.IsSuccessful, Is.True, $"Failed: {(int)HttpbinService.Response.StatusCode} : {HttpbinService.Response.StatusDescription}");
                Assert.That((int)HttpbinService.Response.StatusCode, Is.EqualTo(200));
            });
        }

        [Test]
        public void AuthorizationTest()
        {
            HttpbinService.Authorize("user", "pass");
            Assert.Multiple(() =>
            {
                Assert.That(HttpbinService.Response.IsSuccessful, Is.True, $"Failed: {(int)HttpbinService.Response.StatusCode} : {HttpbinService.Response.StatusDescription}");
                Assert.That((int)HttpbinService.Response.StatusCode, Is.EqualTo(200));
            });
        }

        [Test]
        public void RestAssuredTest()
        {
            new RestAssured()
                .Given()
                    .Name("Simple test using RestAssured")
                .When()
                    .Get($"{Configuration.BaseURL}/ip")
                .Then()
                    .TestBody("test", x => x.origin != null)
                    .Assert("test");
        }
    }
}
