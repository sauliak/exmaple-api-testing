using System;
using System.Collections.Generic;
using System.Text.Json;
using RestSharp;
using RestSharp.Authenticators;
using Serilog;

namespace Httpbin.Client
{
    public class HttpbinService
    {
        private static HttpBasicAuthenticator _httpBasicAuthenticator;
        private static RestRequest _request;
        private static Dictionary<string, object> _result;

        public static RestClient Client { get; set; }

        public static RestResponse Response { get; set; }

        public static RestRequest SetRequest(string url, Method method)
        {
            Log.Information(@$"Request set for ""{url}"" with Method {method}");
            _request = new RestRequest(url, method);
            return _request;
        }

        public static RestResponse SendRequest(string url, Method method)
        {
            Response = Client.ExecuteAsync(SetRequest(url, method)).Result;
            Log.Information(@$"Send request for ""{url}"" with Method {method}");
            return Response;
        }

        public static RestResponse SendRequest(RestRequest request)
        {
            Response = Client.ExecuteAsync(request).Result;
            Log.Information(@$"Send request for ""{request}""");
            return Response;
        }

        public static void GetResponse()
        {
            var response = string.Empty;
            foreach (var kvp in _result)
            {
                response += $"{kvp.Key} : {kvp.Value}\n";
            }

            var stringResult = string.Join(Environment.NewLine, _result);
            Log.Information($"Got response:\n {stringResult}");
        }

        public static int GetStatusCode(int code)
        {
            Log.Information($"Getting status code");
            var request = SetRequest("status/{codes}", Method.Post);
            request.AddUrlSegment("codes", code);
            Response = SendRequest(request);
            Log.Information($"Got status code:\n {(int)Response.StatusCode}");
            return (int)Response.StatusCode;
    }

        public static int GetStatusCodeUsingPatch(int code)
        {
            Log.Information($"Getting status code");
            var request = SetRequest("status/{codes}", Method.Patch);
            request.AddUrlSegment("codes", code);
            Response = SendRequest(request);
            Log.Information($"Got status code:\n {(int)Response.StatusCode}");
            return (int)Response.StatusCode;
        }

        public static RestRequest SetAuthorizationRequest(string username, string password)
        {
            _request = SetRequest("basic-auth/{user}/{passwd}", Method.Get)
                .AddUrlSegment("user", username)
                .AddUrlSegment("passwd", password);
            Log.Information($"Set authorization request for user {username} with password {password}");
            return _request;
        }

        public static void SetAuthenticator(string user, string pass)
        {
            Log.Information($"Set Authenticator");
            _httpBasicAuthenticator = new HttpBasicAuthenticator(user, pass);
        }

        public static string Authenticate()
        {
            var valueTask = _httpBasicAuthenticator.Authenticate(Client, _request);
            Response = SendRequest(_request);
            if (Response.IsSuccessful)
            {
                _result = JsonSerializer.Deserialize<Dictionary<string, object>>(Response.Content);
            }

            var stringResult = string.Join(Environment.NewLine, _result);
            Log.Information($"Got response:\n {stringResult}");
            return stringResult;
        }

        public static string Authorize(string user, string pass)
        {
            SetAuthorizationRequest(user, pass);
            SetAuthenticator(user, pass);
            Log.Information($"Authorize user {user} with password {pass}");
            return Authenticate();
        }

        public static string GetIp()
        {
            Log.Information($"Getting ip");
            SendRequest("ip", Method.Get);
            _result = JsonSerializer.Deserialize<Dictionary<string, object>>(Response.Content);
            var stringResult = string.Join(Environment.NewLine, _result);
            Log.Information($"Got response:\n {stringResult}");
            return stringResult;
        }

        public static string GetHeaders()
        {
            Log.Information($"Gettings headers");
            SendRequest("headers", Method.Get);
            _result = JsonSerializer.Deserialize<Dictionary<string, object>>(Response.Content);
            var stringResult = string.Join(Environment.NewLine, _result);
            Log.Information($"Got response:\n {stringResult}");
            return stringResult;
        }

        public static string GetUserAgent()
        {
            Log.Information($"Gettings user agent");
            SendRequest("user-agent", Method.Get);
            _result = JsonSerializer.Deserialize<Dictionary<string, object>>(Response.Content);
            var stringResult = string.Join(Environment.NewLine, _result);
            Log.Information($"Got response:\n {stringResult}");
            return stringResult;
        }
    }
}
