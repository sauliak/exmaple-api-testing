using NUnit.Framework;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace Httpbin.SpecFlow.Steps
{
    [Binding]
    public class AuthorizationSteps
    {
        [Given(@"I create authorization request for user ""(.*)"" with password ""(.*)""")]
        public static void GivenICreateAuthorizationRequest(string user, string password)
        {
            Client.HttpbinService.SetAuthorizationRequest(user, password);
        }

        [When(@"I put credentials for basic authorization")]
        public static void IPutCredentialsForBasicAuthorization(Table table)
        {
            dynamic credentials = table.CreateDynamicInstance();
            Client.HttpbinService.SetAuthenticator(credentials.Username, credentials.Password);
        }

        [When(@"I send the request")]
        public static void WhenISendTheRequest()
        {
            Client.HttpbinService.Authenticate();
        }

        [Then(@"The authorization should be successful")]
        public static void ThenTheAuthorizationShouldBeSuccessful()
        {
            Assert.That(Client.HttpbinService.Response.IsSuccessful, Is.True, $"Failed: {(int)Client.HttpbinService.Response.StatusCode} : {Client.HttpbinService.Response.StatusDescription}");
        }
    }
}
