using System;
using ApiTests;
using Httpbin.Client;
using TechTalk.SpecFlow;

namespace Httpbin.SpecFlow.Hooks
{
    [Binding]
    public sealed class Hooks
    {
        [BeforeScenario]
        public static void SetUp()
        {
            HttpbinService.Client = new (new Uri(Configuration.BaseURL));
        }

        [AfterScenario]
        public static void TearDown()
        {
            if (HttpbinService.Response.IsSuccessful)
            {
                HttpbinService.GetResponse();
            }

            HttpbinService.Client.Dispose();
        }
    }
}
