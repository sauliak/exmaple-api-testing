﻿Feature: Authorization
	Testing api basic authorization

Scenario: Authorization using credentials
	Given I create authorization request for user "testuser" with password "testpassword"
	When I put credentials for basic authorization
	| Username | Password     |
	| testuser | testpassword |
	And I send the request
	Then The authorization should be successful