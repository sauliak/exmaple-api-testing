using System.IO;
using Microsoft.Extensions.Configuration;

namespace ApiTests
{
    public static class Configuration
    {
        private static readonly IConfigurationRoot _сonfigurationRoot = GetConfiguration();

        public static string Name { get; } = _сonfigurationRoot["Name"];

        public static string BaseURL { get; } = _сonfigurationRoot["BaseURL"];

        private static IConfigurationRoot GetConfiguration()
        {
            return new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", true, true)
                .Build();
        }
    }
}
